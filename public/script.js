$(document).ready(function () {

  const sectionNamesArray = [
    'BIO', 'RESUME', 'EDU_BACKGROUND', 'SKILLS', 'PROJECTS', 'CONTACT', 'WORK'
  ]

  sectionNamesArray.map(sectionName => {
    $("#BTN_" + sectionName).click(function () {
      console.log('clicked')

      sectionNamesArray.map((sectionNameForHiding) => {
        if (sectionNameForHiding !== sectionName) {
          $("#WIN_" + sectionNameForHiding).hide()
        }
      })
      $("#WIN_" + sectionName).fadeIn("fast")
  
    });

  })

  $(".BTN_CLOSE").click(function () {
    $(this).closest(".window").fadeOut("fast", function () { });
  });
});


